package controllers

import (
	"github.com/astaxie/beego"
	"io/ioutil"
)

// operations for FollowerList
type FollowerListController struct {
	beego.Controller
}

func (c *FollowerListController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// @Title Post
// @Description create FollowerList
// @Param	body		body 	models.FollowerList	true		"body for FollowerList content"
// @Success 200 {int} models.FollowerList.Id
// @Failure 403 body is empty
// @router / [post]
func (c *FollowerListController) Post() {
	buff, err := ioutil.ReadFile("json/friend.json")
	if err != nil {
		panic("open file failed!")
	}
	c.Ctx.Output.Body(buff)

}

// @Title Get
// @Description get FollowerList by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.FollowerList
// @Failure 403 :id is empty
// @router /:id [get]
func (c *FollowerListController) GetOne() {

}

// @Title Get All
// @Description get FollowerList
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.FollowerList
// @Failure 403
// @router / [get]
func (c *FollowerListController) GetAll() {
	buff, err := ioutil.ReadFile("json/friend.json")
	if err != nil {
		panic("open file failed!")
	}
	c.Ctx.Output.Body(buff)
}

// @Title Update
// @Description update the FollowerList
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.FollowerList	true		"body for FollowerList content"
// @Success 200 {object} models.FollowerList
// @Failure 403 :id is not int
// @router /:id [put]
func (c *FollowerListController) Put() {

}

// @Title Delete
// @Description delete the FollowerList
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *FollowerListController) Delete() {

}
