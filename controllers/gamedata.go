package controllers

import (
	"github.com/astaxie/beego"
	"io/ioutil"
)

// operations for GameData
type GameDataController struct {
	beego.Controller
}

func (c *GameDataController) URLMapping() {
	c.Mapping("Post", c.Post)
	//c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	//c.Mapping("Put", c.Put)
	//c.Mapping("Delete", c.Delete)
}

// @Title Post
// @Description create GameData
// @Param	body		body 	models.GameData	true		"body for GameData content"
// @Success 200 {int} models.GameData.Id
// @Failure 403 body is empty
// @router / [post]
func (c *GameDataController) Post() {
	dataVer, _ := c.GetInt("dataVer")
	buff, err := ioutil.ReadFile("json/gamedatanone.json")
	if dataVer > 0 {
		if err != nil {
			panic("open file failed!")
		}
	}else{
		buff, err = ioutil.ReadFile("json/gamedata.json")
		if err != nil {
			panic("open file failed!")
		}
	}
	c.Ctx.Output.Body(buff)
}

// @Title Get
// @Description get GameData by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.GameData
// @Failure 403 :id is empty
// @router /:id [get]
func (c *GameDataController) GetOne() {

}

// @Title Get All
// @Description get GameData
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.GameData
// @Failure 403
// @router / [get]
func (c *GameDataController) GetAll() {
	buff, err := ioutil.ReadFile("json/gamedata.json")
	if err != nil {
		panic("open file failed!")
	}
	c.Ctx.Output.Body(buff)

}

// @Title Update
// @Description update the GameData
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.GameData	true		"body for GameData content"
// @Success 200 {object} models.GameData
// @Failure 403 :id is not int
// @router /:id [put]
func (c *GameDataController) Put() {

}

// @Title Delete
// @Description delete the GameData
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *GameDataController) Delete() {

}
