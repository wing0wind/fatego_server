package controllers

import (
	"github.com/astaxie/beego"
	"io/ioutil"
	//"os"
    //"log"
)

// operations for Register
type RegisterController struct {
	beego.Controller
}

func (c *RegisterController) URLMapping() {
	c.Mapping("Post", c.Post)
	//c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	//c.Mapping("Put", c.Put)
	//c.Mapping("Delete", c.Delete)
}

// @Title Post
// @Description create Register
// @Param	body		body 	models.Register	true		"body for Register content"
// @Success 201 {object} models.Register
// @Failure 403 body is empty
// @router / [post]
func (c *RegisterController) Post() {
	buff, err := ioutil.ReadFile("json/reg.json")
	if err != nil {
		panic("open file failed!")
	}
	c.Ctx.Output.Body(buff)

}

// @Title Get
// @Description get Register by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Register
// @Failure 403 :id is empty
// @router /:id [get]
func (c *RegisterController) GetOne() {
	
}

// @Title Get All
// @Description get Register
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Register
// @Failure 403
// @router / [get]
func (c *RegisterController) GetAll() {
	//file, _ := os.Getwd()
    //log.Println("current path:", file)
	buff, err := ioutil.ReadFile("json/reg.json")
	if err != nil {
		panic("open file failed!")
	}
	//type M map[string]interface{}
	//dic := M{"Name": "Bob", "Phone": M{"aaa": "bbb"}}
	c.Ctx.Output.Body(buff)
}

// @Title Update
// @Description update the Register
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Register	true		"body for Register content"
// @Success 200 {object} models.Register
// @Failure 403 :id is not int
// @router /:id [put]
func (c *RegisterController) Put() {

}

// @Title Delete
// @Description delete the Register
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *RegisterController) Delete() {

}
