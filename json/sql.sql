＃bee generate appcode -driver=mysql -conn="root:@tcp(127.0.0.1:3306)/fatego" -level=1
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `createAt` datetime NOT NULL,
  `updateAt` date DEFAULT NULL,
  `actRecoverAt` date DEFAULT NULL ,
  `commandSpellRecoverAt` date DEFAULT NULL ,

  `friendCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genderType` int(11) UNSIGNED DEFAULT NULL ,
  `stone` int(11) UNSIGNED DEFAULT NULL ,
  `svtKeep` int(11) UNSIGNED DEFAULT NULL ,
  `svtEquipKeep` int(11) UNSIGNED DEFAULT NULL ,
  `birthDay` int(11) UNSIGNED DEFAULT NULL ,
  `actMax` int(11) UNSIGNED DEFAULT NULL ,

  `carryOverActPoint` int(11) UNSIGNED DEFAULT NULL ,
  `lv` int(11) UNSIGNED DEFAULT NULL ,
  `exp` int(11) UNSIGNED DEFAULT NULL ,
  `qp` int(11) UNSIGNED DEFAULT NULL ,
  `costMax` int(11) UNSIGNED DEFAULT NULL ,

  `favoriteUserSvtId` bigint(21) UNSIGNED DEFAULT NULL ,

  `userEquipId` int(11) UNSIGNED DEFAULT NULL ,
  `freeStone` int(11) UNSIGNED DEFAULT NULL ,
  `chargeStone` int(11) UNSIGNED DEFAULT NULL ,
  `mana` int(11) UNSIGNED DEFAULT NULL ,
  `mainDeckId` int(11) UNSIGNED DEFAULT NULL ,
  `activeDeckId` int(11) UNSIGNED DEFAULT NULL ,
  `tutorial1` int(11) UNSIGNED DEFAULT NULL ,
  `tutorial2` int(11) UNSIGNED DEFAULT NULL ,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `friendCode` (`friendCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
