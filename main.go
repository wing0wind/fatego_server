package main

import (
	_ "fatego/docs"
	_ "fatego/routers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego/context"
)

func init() {
	orm.RegisterDataBase("default", "mysql", "root:@tcp(127.0.0.1:3306)/fatego")
}

//Check session
//Becaful you must set sessionon = true,or app will crash
func sessionFilter(){
	var FilterUser = func(ctx *context.Context) {
    _, ok := ctx.Input.Session("uid").(int)
    if !ok && ctx.Request.RequestURI != "/v1/account/regist" {
        //TODO return a error infomation json file.
		ctx.Redirect(302, "/v1/account/regist")
    	}
	}
	beego.InsertFilter("/*",beego.BeforeRouter,FilterUser)
}

//Filters
func setFilters(){	
	sessionFilter()	
}

func main() {
	if beego.RunMode == "dev" {
		beego.DirectoryIndex = true
		beego.StaticDir["/swagger"] = "swagger"
	}
	
	setFilters()
	
	beego.Run()
}

