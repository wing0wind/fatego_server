package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["fatego/controllers:BattleResultController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleResultController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleResultController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleResultController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleResultController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleResultController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleResultController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleResultController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleResultController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleResultController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"] = append(beego.GlobalControllerRouter["fatego/controllers:BattleSetupController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:FollowerListController"] = append(beego.GlobalControllerRouter["fatego/controllers:FollowerListController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:FollowerListController"] = append(beego.GlobalControllerRouter["fatego/controllers:FollowerListController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:FollowerListController"] = append(beego.GlobalControllerRouter["fatego/controllers:FollowerListController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:FollowerListController"] = append(beego.GlobalControllerRouter["fatego/controllers:FollowerListController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:FollowerListController"] = append(beego.GlobalControllerRouter["fatego/controllers:FollowerListController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:GameDataController"] = append(beego.GlobalControllerRouter["fatego/controllers:GameDataController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:GameDataController"] = append(beego.GlobalControllerRouter["fatego/controllers:GameDataController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:GameDataController"] = append(beego.GlobalControllerRouter["fatego/controllers:GameDataController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:GameDataController"] = append(beego.GlobalControllerRouter["fatego/controllers:GameDataController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:GameDataController"] = append(beego.GlobalControllerRouter["fatego/controllers:GameDataController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:HomeController"] = append(beego.GlobalControllerRouter["fatego/controllers:HomeController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:HomeController"] = append(beego.GlobalControllerRouter["fatego/controllers:HomeController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:HomeController"] = append(beego.GlobalControllerRouter["fatego/controllers:HomeController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:HomeController"] = append(beego.GlobalControllerRouter["fatego/controllers:HomeController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:HomeController"] = append(beego.GlobalControllerRouter["fatego/controllers:HomeController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:LoginController"] = append(beego.GlobalControllerRouter["fatego/controllers:LoginController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:LoginController"] = append(beego.GlobalControllerRouter["fatego/controllers:LoginController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:LoginController"] = append(beego.GlobalControllerRouter["fatego/controllers:LoginController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:LoginController"] = append(beego.GlobalControllerRouter["fatego/controllers:LoginController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:LoginController"] = append(beego.GlobalControllerRouter["fatego/controllers:LoginController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:RegisterController"] = append(beego.GlobalControllerRouter["fatego/controllers:RegisterController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:RegisterController"] = append(beego.GlobalControllerRouter["fatego/controllers:RegisterController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:RegisterController"] = append(beego.GlobalControllerRouter["fatego/controllers:RegisterController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:RegisterController"] = append(beego.GlobalControllerRouter["fatego/controllers:RegisterController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:RegisterController"] = append(beego.GlobalControllerRouter["fatego/controllers:RegisterController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:SignupController"] = append(beego.GlobalControllerRouter["fatego/controllers:SignupController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:SignupController"] = append(beego.GlobalControllerRouter["fatego/controllers:SignupController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:SignupController"] = append(beego.GlobalControllerRouter["fatego/controllers:SignupController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:SignupController"] = append(beego.GlobalControllerRouter["fatego/controllers:SignupController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:SignupController"] = append(beego.GlobalControllerRouter["fatego/controllers:SignupController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:UserController"] = append(beego.GlobalControllerRouter["fatego/controllers:UserController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:UserController"] = append(beego.GlobalControllerRouter["fatego/controllers:UserController"],
		beego.ControllerComments{
			"GetOne",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:UserController"] = append(beego.GlobalControllerRouter["fatego/controllers:UserController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:UserController"] = append(beego.GlobalControllerRouter["fatego/controllers:UserController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["fatego/controllers:UserController"] = append(beego.GlobalControllerRouter["fatego/controllers:UserController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

}
