// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"fatego/controllers"

	"github.com/astaxie/beego"
)

func init() {	
	ns := beego.NewNamespace("/v1",

		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),	
		),
		beego.NSRouter("/account/regist", &controllers.RegisterController{}),
		beego.NSRouter("/gamedata/top", &controllers.GameDataController{},),
		beego.NSRouter("/login/top", &controllers.LoginController{}),
		beego.NSRouter("/home/top", &controllers.HomeController{}),
		beego.NSRouter("/signup/top", &controllers.SignupController{}),
		beego.NSRouter("/follower/list", &controllers.FollowerListController{}),
		beego.NSRouter("/battle/setup", &controllers.BattleSetupController{}),
		beego.NSRouter("/battle/result", &controllers.BattleResultController{}),
	)
	beego.AddNamespace(ns)
}
